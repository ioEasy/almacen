﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrupoSM_Recepcion.GUI.REPORTES
{
 public partial class TelasRequisicion:Form
 {


  public TelasRequisicion()
  {
  InitializeComponent();
this.WindowState=FormWindowState.Maximized;
  }
public int produccion { get; set; }

  private void TelasRequisicion_Load(object sender,EventArgs e)
  {
DAO.Oden_ProduccionDAO ordenproducciondao = new DAO.Oden_ProduccionDAO();
  ordenproducciondao.idorden=produccion;
  DAO.TelasDAO telasdao = new DAO.TelasDAO();
  telasdao.produccion=this.produccion;
  GUI.PLANTILLAS.RequisicionCosturas report = new PLANTILLAS.RequisicionCosturas();
  report.SetDataSource(telasdao.telasrequisicionorden());
  report.Subreports["Telas"].SetDataSource(telasdao.telasrequisicionorden());
report.Subreports["Encabezado"].SetDataSource(ordenproducciondao.encabezadodepaginas());
  report.Subreports["Combinaciones"].SetDataSource(telasdao.combinacionesrequisicionorden());
  report.Subreports["Forros"].SetDataSource(telasdao.forrosrequisicionorden());
  crystalReportViewer1.ReportSource = report;
  crystalReportViewer1.Refresh(); 
  }
 }
}
