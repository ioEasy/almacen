﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrupoSM_Recepcion.GUI.REPORTES
{
 public partial class HojaDeCorteAtras:Form
 {
  public HojaDeCorteAtras()
  {
  InitializeComponent();
this.WindowState = FormWindowState.Maximized;
  }

public int idproduccion { get; set; }
  private void HojaDeCorteAtras_Load(object sender,EventArgs e)
  {	
DAO.PiezasDAO piezasdao = new GrupoSM_Recepcion.DAO.PiezasDAO();

piezasdao.orden = this.idproduccion;

  DAO.AviosDAO aviosdao = new GrupoSM_Recepcion.DAO.AviosDAO();
aviosdao.idproduccion = this.idproduccion;


DAO.TelasDAO telasdao = new GrupoSM_Recepcion.DAO.TelasDAO();
telasdao.id_tela_produccion = this.idproduccion;

  GUI.PLANTILLAS.CrystalReportHojaCorte report = new GUI.PLANTILLAS.CrystalReportHojaCorte();
            
//report.SetDataSource(telasdao.devuelvetelashojadecorte());
report.Subreports["Telas"].SetDataSource(telasdao.devuelvetelashojadecorte());
report.Subreports["Avios"].SetDataSource(aviosdao.devuelvehojadecorteavios());
report.Subreports["Piezas"].SetDataSource(piezasdao.devuelveordentrabajoseparado());
  
            
crystalReportViewer1.ReportSource = report;
            
crystalReportViewer1.Refresh();
  }
 }
}
