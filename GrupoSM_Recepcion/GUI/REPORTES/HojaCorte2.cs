﻿using System;
using System.Windows.Forms;

namespace GrupoSM_Recepcion.GUI.REPORTES
{
public partial class HojaCorte2 : Form
{
public HojaCorte2()
{
InitializeComponent();
this.WindowState = FormWindowState.Maximized;
}
public int idficha { get; set; }
public int idproduccion { get; set; }

private void HojaCorte2_Load(object sender, EventArgs e)
{			
DAO.PiezasDAO piezasdao = new GrupoSM_Recepcion.DAO.PiezasDAO();

piezasdao.orden = this.idproduccion;

DAO.AviosDAO aviosdao = new GrupoSM_Recepcion.DAO.AviosDAO();
aviosdao.id_ficha_avio = this.idficha;
aviosdao.idproduccion = this.idproduccion;
DAO.Ficha_tecnicaDAO fichasdao = new GrupoSM_Recepcion.DAO.Ficha_tecnicaDAO();
fichasdao.id_fichatecnica=this.idficha;

DAO.TelasDAO telasdao = new GrupoSM_Recepcion.DAO.TelasDAO();
telasdao.id_tela_produccion = this.idproduccion;

GUI.PLANTILLAS.ReversoHojaDeCorte report = new GUI.PLANTILLAS.ReversoHojaDeCorte();
            
report.SetDataSource(telasdao.devuelvetelashojadecorte());
report.Subreports["Telas"].SetDataSource(telasdao.devuelvetelashojadecorte());
report.Subreports["Avios"].SetDataSource(aviosdao.devuelvehojadecorteavios());
report.Subreports["Piezas"].SetDataSource(piezasdao.devuelveordentrabajoseparado());
  
            
crystalReportViewer1.ReportSource = report;
            
crystalReportViewer1.Refresh();
}
}
}
