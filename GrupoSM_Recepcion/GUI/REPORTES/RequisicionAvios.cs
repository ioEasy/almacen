﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrupoSM_Recepcion.GUI.REPORTES
{
public partial class RequisicionAvios:Form
{
public int idproduccion { get; set; }
public RequisicionAvios()
{
InitializeComponent();
this.WindowState=FormWindowState.Maximized;
}

private void RequisicionAvios_Load(object sender,EventArgs e)
{

GUI.PLANTILLAS.RequisicionAviosrpt report = new PLANTILLAS.RequisicionAviosrpt();

DAO.AviosDAO aviosdao = new DAO.AviosDAO();
DAO.Oden_ProduccionDAO ordenproducciondao = new DAO.Oden_ProduccionDAO();
  ordenproducciondao.idorden=idproduccion;
aviosdao.idproduccion=this.idproduccion;

report.SetDataSource(aviosdao.devuelverequisiciontabla());
  report.Subreports["Encabezado"].SetDataSource(ordenproducciondao.encabezadodepaginas());
crystalReportViewer1.ReportSource=report;
crystalReportViewer1.Refresh();


}
}
}

