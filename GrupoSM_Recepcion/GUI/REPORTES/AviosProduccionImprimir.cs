﻿using System;
using System.Windows.Forms;

namespace GrupoSM_Recepcion.GUI.REPORTES
{
    public partial class AviosProduccionImprimir : Form
    {
        public AviosProduccionImprimir()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
        }

        //    DAO.AviosDAO aviosdao = new GrupoSM_Recepcion.DAO.AviosDAO();
        //    GUI.Bodega.Avios_captura avioscapturagui = new Avios_captura();
        //    aviosdao.id_ficha_avio = (Convert.ToInt16(dataGridView1.CurrentRow.Cells["idficha"].Value));
        //    DAO.Oden_ProduccionDAO ordendao = new GrupoSM_Recepcion.DAO.Oden_ProduccionDAO();
        //    ordendao.idorden = (Convert.ToInt16(dataGridView1.CurrentRow.Cells["id_orden"].Value));
        //    avioscapturagui.textBox2.Text = Convert.ToString(ordendao.devuelve_numeroprendas());
        //    avioscapturagui.label5.Text = Convert.ToString(dataGridView1.CurrentRow.Cells["id_orden"].Value);
            
        //    avioscapturagui.dataGridView1.DataSource = aviosdao.sacar_avios();
        public int idproduccion { get; set; }
        public int idficha { get; set; }
        


        //    avioscapturagui.ShowDialog();

        private void AviosProduccionImprimir_Load(object sender, EventArgs e)
        {
            DAO.AviosDAO aviosdao = new GrupoSM_Recepcion.DAO.AviosDAO();
            //;
            aviosdao.idproduccion = this.idproduccion;
            DAO.Oden_ProduccionDAO ordendao = new GrupoSM_Recepcion.DAO.Oden_ProduccionDAO();
            ordendao.idorden = (this.idproduccion);
            //ReportDocument report = new ReportDocument();
            //GUI.PLANTILLAS.AviosRequisicionSubRPT report = new GrupoSM_Recepcion.GUI.PLANTILLAS.AviosRequisicionSubRPT();
            
            //report.SetDataSource(aviosdao.aviosimpresion());
            //report.Subreports["NumeroprendasRPT"].SetDataSource(ordendao.numeroprendasreporte());
            //report.Subreports["AviosSubRPT"].SetDataSource(aviosdao.sacar_avios());
            //report.Subreports[0].SetDataSource(ordendao.datosproduccionreporte());
            //crystalReportViewer1.ReportSource = report;
            crystalReportViewer1.Refresh();



        }
    }
}
