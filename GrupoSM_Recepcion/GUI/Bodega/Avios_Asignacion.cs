﻿using System;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;
using System.Resources; 

namespace GrupoSM_Recepcion.GUI.Bodega
{
public partial class Avios_Asignacion : Form
{

public void verificaavios()
{
int comprobacion;
DAO.AviosDAO aviosdao1 = new GrupoSM_Recepcion.DAO.AviosDAO();
aviosdao1.idproduccion=int.Parse(label6.Text);
comprobacion=aviosdao1.verificaavios();
aviosdao1.actualizaaproduccionavioscompletos();
if(comprobacion==0)
{
MessageBox.Show("Es necesario primero guardar el listado de avios que se utilizaran en la prenda");

} else
{
button2.Enabled=true;

}
}
public void devuelvecampos()
{
dataGridView5.Visible=false;

DAO.AviosDAO aviosdao = new GrupoSM_Recepcion.DAO.AviosDAO();

dataGridView5.DataSource=aviosdao.almacenavioslistado();

aviosdao.idproduccion=int.Parse(label6.Text);

dataGridView3.DataSource=aviosdao.devuelvehojadecorteavios();

aviosdao.idproduccion=int.Parse(label6.Text);

dataGridView1.DataSource=aviosdao.devuelveaviosasignaciones();
cargacombobox();

dataGridView2.DataSource=Generardatagrid();cargacombobox();
}
public BindingSource Generardatagrid()
{
DAO.AviosDAO aviosdao = new DAO.AviosDAO();

BindingSource source = new BindingSource();

string nombre="", tipo="";
int clave=0, id=0;


foreach(DataGridViewRow row in dataGridView5.Rows)
{

aviosdao.nombre=row.Cells[0].Value.ToString();
dataGridView4.DataSource=aviosdao.datosalmacen();
nombre=row.Cells[0].Value.ToString();
foreach(DataGridViewRow row1 in dataGridView4.Rows)
{
tipo=row1.Cells[0].Value.ToString();
id=Convert.ToInt16(row1.Cells[1].Value.ToString());
clave=Convert.ToInt16(row1.Cells[2].Value.ToString());
}
source.Add(new Services.Avios.Avio(nombre,tipo,clave,id));
}
return source;

}
public void cargacombobox()
{
comboBox3.Items.Clear();
foreach(DataGridViewRow row in dataGridView2.Rows)
{
comboBox3.Items.Add(row.Cells[2].Value.ToString());
}
comboBox3.SelectedIndex=-1;
}
public void ingresaalmacen()
{

if(textBox7.Text!="")
{
DAO.AviosDAO aviosdao1 = new GrupoSM_Recepcion.DAO.AviosDAO();
aviosdao1.idproduccion=int.Parse(label6.Text);
aviosdao1.cantidadd=int.Parse(textBox7.Text);
aviosdao1.tipoo=comboBox2.Text;
aviosdao1.nombre=comboBox3.Text;
string resultado = aviosdao1.insertaalmacen();
if(resultado=="Correcto")
{
decimal ingresado;
try
{
ingresado = Convert.ToDecimal(dataGridView1.CurrentRow.Cells["Ingresado"].Value);
}
catch
{
ingresado = 0;
}
aviosdao1.cantidadd=decimal.Parse(textBox7.Text)+ingresado;
aviosdao1.idavioproduccion=Convert.ToInt32(dataGridView1.CurrentRow.Cells["Codigo"].Value);
aviosdao1.fecha=dateTimePicker1.Value;
aviosdao1.cantidadbodegaa=aviosdao1.numerobodegaavios();
aviosdao1.ingresaprimeralmacen();
} 
else
{
MessageBox.Show(resultado);
}
}
limpiatextos();
/* else
 {
  DAO.AviosDAO aviosdao1 = new GrupoSM_Recepcion.DAO.AviosDAO();
  aviosdao1.idproduccion=int.Parse(label6.Text);
  aviosdao1.cantidadd=int.Parse(textBox2.Text);
  aviosdao1.tipoo=dataGridView2.CurrentRow.Cells["Tipo"].Value.ToString();
  aviosdao1.nombre=dataGridView2.CurrentRow.Cells["Nombre"].Value.ToString();
  string resultado = aviosdao1.insertaalmacen();
  if(resultado=="Correcto")
  {
  if(resultado=="Correcto")
  {
  decimal ingresado;
  try
  {
  ingresado=Convert.ToDecimal(dataGridView1.CurrentRow.Cells["Ingresado"].Value);
  } catch
  {
  ingresado=0;
  }
  aviosdao1.cantidadd=decimal.Parse(textBox2.Text)+ingresado;
  aviosdao1.idavioproduccion=Convert.ToInt32(dataGridView1.CurrentRow.Cells["Codigo"].Value);
  aviosdao1.cantidadbodegaa=aviosdao1.numerobodegaavios();
  aviosdao1.ingresaalmacencatalogo();
  aviosdao1.ingresaprimeralmacen();
  } else
  {
  MessageBox.Show(resultado);
  }
  }
  limpiatextos();
  }
*/
}

public void limpiatextos()
{
comboBox2.SelectedIndex=-1;
textBox7.Text="";
textBox2.Text="";
}
public void guardanumeroprendas()
{
DAO.AviosDAO aviosdao = new GrupoSM_Recepcion.DAO.AviosDAO();
aviosdao.idproduccion=int.Parse(label6.Text);
aviosdao.Color=dataGridView1.CurrentRow.Cells["Color"].Value.ToString();
label2.Text=aviosdao.numerocolorprendas().ToString();
}
public DataView generatablabusqueda()
{
BO.DS_TablasDelSistema ds = new BO.DS_TablasDelSistema();
DataView dv = new DataView(ds.Tables["CatalogoDeAlmacen"]);
foreach(DataGridViewRow row in dataGridView2.Rows)
{
DataRowView newRow = dv.AddNew();
newRow["Nombre"]=row.Cells[2].Value.ToString();
newRow["Clave"]=row.Cells[0].Value.ToString();
newRow["Tipo"]=row.Cells[1].Value.ToString();
newRow["ID"]=row.Cells[3].Value.ToString();
newRow.EndEdit();
}
string campo = "Nombre ";

dv.RowFilter=campo+" like '%"+textBox5.Text+"%'";

return dv;
}
public int calculabodegacompleta()
{
int devuelve = 0, cantidad = 0;
foreach(DataGridViewRow row in dataGridView1.Rows)
{
decimal cantidad0 = Convert.ToDecimal(row.Cells["Almacen"].Value),
cantidad1 = Convert.ToDecimal(row.Cells["Requerido"].Value);
if(cantidad0<cantidad1)
{
devuelve+=1;
} else
{
cantidad+=1;
}
}
if(cantidad!=0)
{
return 1;
} else
{
return 0;
}
  }

public Avios_Asignacion()
{
InitializeComponent();
}					
private void Avios_Asignacion_Load(object sender, EventArgs e)
{	
verificaavios();
devuelvecampos();			
} 
private void button2_Click(object sender, EventArgs e)
{									
}										
private void button1_Click(object sender, EventArgs e)
{												
}										
private void textBox2_TextChanged(object sender, EventArgs e)
{
button1.Visible = true;
}							
private void dataGridView1_DoubleClick(object sender, EventArgs e)
{       
}				
private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
{													
}											
private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
{
}								

private void button2_Click_1(object sender, EventArgs e)
{
string mensaje;
DialogResult result;
mensaje="¿"+"De verdad desea agregar el avio "+comboBox3.Text+" por el avio "+dataGridView1.CurrentRow.Cells[0].Value.ToString()+"?"+" Tenga en cuenta que si esta incorrecto el avio tendra que reingresar los datos de nuevo";
result = MessageBox.Show(mensaje,"Confirmacion",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
if(result==DialogResult.Yes)
{
ingresaalmacen();
devuelvecampos();
if(calculabodegacompleta()==0)
{
DAO.AviosDAO aviosdao = new DAO.AviosDAO();
aviosdao.idproduccion=int.Parse(label6.Text);

if(aviosdao.actualizaordendeproduccionalmacencompleto()=="Correcto")
{
MessageBox.Show("Correcto");
}
}
}
           
}
private void button3_Click(object sender, EventArgs e)
{        
}				
private void textBox5_TextChanged(object sender, EventArgs e)
{
DataView dv = generatablabusqueda();
dataGridView2.DataSource = dv;
}
private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
{				
}				
private void button4_Click(object sender, EventArgs e)
{      
}
private void Avios_Enter(object sender, EventArgs e)
{

}
private void comboBox3_KeyPress_1(object sender, KeyPressEventArgs e)
{
string str = e.KeyChar.ToString().ToUpper();
char[] ch = str.ToCharArray();
e.KeyChar = ch[0];
  textBox7.Enabled=true;
}
private void comboBox3_KeyPress(object sender, KeyPressEventArgs e)
{
string str = e.KeyChar.ToString().ToUpper();
char[] ch = str.ToCharArray();
e.KeyChar = ch[0];
}

private void button3_Click_1(object sender, EventArgs e)
{
DialogResult result = MessageBox.Show("Tenga en cuenta que la entrada del avio se eliminara de bodega y de catalogo", "Confirmacion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
if(result==DialogResult.Yes)
{
DAO.AviosDAO aviosdao = new DAO.AviosDAO();
aviosdao.IDAvioAlmacen = Convert.ToInt32(dataGridView2.CurrentRow.Cells["ID"].Value);
aviosdao.eliminadebodegaalmacen();
devuelvecampos();		
}
}

  private void button4_Click_1(object sender,EventArgs e)
  {
  string mensaje;
  DialogResult result;
  mensaje="¿"+"De verdad desea agregar el avio "+comboBox3.Text+" por el avio "+dataGridView1.CurrentRow.Cells[0].Value.ToString()+"?"+" Tenga en cuenta que si esta incorrecto el avio tendra que reingresar los datos de nuevo";
  result = MessageBox.Show(mensaje,"Confirmacion",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
  if(result==DialogResult.Yes)
  {
  ingresaalmacen();
  devuelvecampos();
  if(calculabodegacompleta()==0)
  {
  DAO.AviosDAO aviosdao = new DAO.AviosDAO();
  aviosdao.idproduccion=int.Parse(label6.Text);

  if(aviosdao.actualizaordendeproduccionalmacencompleto()=="Correcto")
  {
  MessageBox.Show("Correcto");
  }
  }
  }
  }
 }
}
