﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrupoSM_Recepcion.GUI.Bodega
{
    public partial class ImpresionesAvios : Form
    {
        public ImpresionesAvios()
        {
            InitializeComponent();
        }
        public void devuelvecatalogo()
        {
            DAO.AviosDAO aviosdao = new DAO.AviosDAO();

            dataGridView2.DataSource = aviosdao.existenciascatalogoalmacenavios();
        }
        
        public void devuelvecatalogobodega()
        {
            DAO.AviosDAO AVIOSDAO = new DAO.AviosDAO();
            //dataGridView2.DataSource = AVIOSDAO.aviosproducciontabla();
        }
        
        public void generaformatoreporte1()
        {
            devuelvecatalogo();

            BindingSource source = new BindingSource();

            string nombre, tipo;
            int id;
            decimal cantidad;

            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                DAO.AviosDAO aviosdao = new DAO.AviosDAO();

                aviosdao.nombre = dataGridView2.CurrentRow.Cells[0].Value.ToString();

                dataGridView3.DataSource = aviosdao.datoscatalogoalmacenavios();

                nombre = row.Cells[0].Value.ToString();
                cantidad = Convert.ToDecimal(row.Cells[1].Value.ToString());
                foreach (DataGridViewRow row1 in dataGridView3.Rows)
                {
                    tipo = row1.Cells[2].Value.ToString();

                    id = Convert.ToInt16(row1.Cells[0].Value);
                    source.Add(new Services.AviosAlmacen.AvioAlmacen(nombre, tipo, cantidad, id));
                }
            }

            dataGridView1.DataSource = source;
        }
        public void generaformatoreporte2()
        {
            devuelvecatalogobodega();
            BindingSource source = new BindingSource();
            string nombre, tipo, color;
            decimal cantidad;
            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                foreach (DataGridViewRow row1 in dataGridView3.Rows)
                {
                    DAO.AviosDAO aviosdao = new DAO.AviosDAO();
                    aviosdao.idavios = int.Parse(dataGridView2.CurrentRow.Cells[1].Value.ToString());
                    //dataGridView3.DataSource = aviosdao.aviosproducciontablanombre();

                    nombre = row1.Cells[0].Value.ToString();
                    cantidad = Convert.ToDecimal(row.Cells[1].Value.ToString());
                    tipo = row1.Cells[1].Value.ToString();
                    color = Convert.ToString(row.Cells[2].Value);
                    source.Add(new Services.AviosAlmacen.AvioBodega(nombre, tipo, cantidad, color));
                }
            }

            dataGridView1.DataSource = source;
        }
    private void button1_Click(object sender, EventArgs e)
    {
        if (comboBox1.SelectedIndex == 0)
        {
            generaformatoreporte1();
        }
        if (comboBox1.SelectedIndex == 1)
        {
            generaformatoreporte2();
        }
    }


    private void button2_Click(object sender, EventArgs e)
    {
        devuelvecatalogo();
    }

    private void button3_Click(object sender, EventArgs e)
    {
        //devuelvecatalogopornombre();
    }
}
}


