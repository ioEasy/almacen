﻿using System;
using System.Windows.Forms;

namespace GrupoSM_Recepcion.GUI.Bodega
{
public partial class Telas_Captura : Form
{
public Telas_Captura()
{
InitializeComponent();
}

private void Telas_Captura_Load(object sender, EventArgs e)
{
DAO.ProduccionDAO producciondao = new GrupoSM_Recepcion.DAO.ProduccionDAO();
producciondao.id_produccion = int.Parse(lbl_idorden.Text);
//dataGridView1.DataSource = producciondao.combinacionproduccion();
//dataGridView2.DataSource = producciondao.vertelasproduccionsum();
double sumatoria = 0;

foreach (DataGridViewRow row in dataGridView1.Rows)
{
sumatoria += Convert.ToDouble(row.Cells["cantidad_prendas"].Value);
}

tb_cantidad.Text = Convert.ToString(sumatoria);
tb_metroforro.Text = Convert.ToString(double.Parse(tb_cantidad.Text) * double.Parse(lbl_consumoforro.Text));
cargadatagrids ();


}

private void button2_Click(object sender, EventArgs e)
{
           
}
public void cargadatagrids()
{
DAO.TelasDAO telasdao = new GrupoSM_Recepcion.DAO.TelasDAO ();
telasdao.produccion = int.Parse ( lbl_idorden.Text );
telasdao.tipoo = "Tela";
dataGridView3.DataSource = telasdao.telasportipodeproduccion ();
telasdao.tipoo = "Combinacion";
dataGridView4.DataSource = telasdao.telasportipodeproduccion ();
telasdao.tipoo = "Forro";
dataGridView5.DataSource = telasdao.telasportipodeproduccion ();
}
private void button2_Click_1(object sender, EventArgs e)
{
tb_cantidadcombinacion.Text = Convert.ToString(dataGridView1.CurrentRow.Cells["cantidad_prendas"].Value);
tb_metroscombinacion.Text = Convert.ToString(double.Parse(tb_cantidadcombinacion.Text) * double.Parse(lbl_consumocombinacion.Text));
}

private void button3_Click(object sender, EventArgs e)
{
GUI.Bodega.Tela_Seleccion telasgui = new Tela_Seleccion();
telasgui.label1.Text = lbl_cliente.Text;
telasgui.label2.Text = "0";
telasgui.label6.Text = label1.Text;
telasgui.tb_combinacion.Text = tb_tela.Text;
telasgui.label5.Text = "Color";
telasgui.tb_cantidadcombinacion.Text = Convert.ToString(dataGridView2.CurrentRow.Cells["color"].Value);
telasgui.tb_metroscombinacion.Text = Convert.ToString(dataGridView2.CurrentRow.Cells["metros"].Value);
telasgui.label3.Text = lbl_idorden.Text;
telasgui.ShowDialog();
cargadatagrids ();	
}

private void button4_Click(object sender, EventArgs e)
{
GUI.Bodega.Tela_Seleccion telasgui = new Tela_Seleccion();
telasgui.label1.Text = lbl_cliente.Text;
telasgui.label2.Text = "1";
telasgui.label6.Text = label1.Text;
telasgui.tb_combinacion.Text = tb_tela.Text;
telasgui.label5.Visible = false;
telasgui.tb_cantidadcombinacion.Visible = false;
telasgui.label4.Visible = false;
telasgui.tb_metroscombinacion.Visible = false;
telasgui.label3.Text = lbl_idorden.Text;
telasgui.ShowDialog();
cargadatagrids ();
}

private void button5_Click(object sender, EventArgs e)
{
if ((tb_forro.Text != "") && (tb_metroforro.Text != "") && (tb_cantidad.Text != ""))
{
GUI.Bodega.Tela_Seleccion telasgui = new Tela_Seleccion();
telasgui.label1.Text = lbl_cliente.Text;
telasgui.label2.Text = "2";
telasgui.label6.Text = label9.Text;
telasgui.tb_combinacion.Text = tb_forro.Text;
telasgui.label5.Visible = false;
telasgui.tb_cantidadcombinacion.Visible = false;
telasgui.label4.Visible = false;
telasgui.tb_metroscombinacion.Visible = false;
telasgui.label3.Text = lbl_idorden.Text;
telasgui.ShowDialog();
cargadatagrids ();

}
else
{
button5.Enabled = false;
}
}
public decimal metrosdetela()
{
DAO.TelasDAO telasdao=new DAO.TelasDAO();
telasdao.produccion=	int.Parse(lbl_idorden.Text);
return telasdao.cantidaddemetrosdetelaporordendeproduccion();
}
private void button6_Click(object sender, EventArgs e)
{
          
decimal sumatoria = metrosdetela();
decimal sumatoria2 = 0;
foreach (DataGridViewRow row in dataGridView3.Rows)
{
sumatoria2 += Convert.ToDecimal(row.Cells["Metros"].Value);
}
textBox1.Text = Convert.ToString(sumatoria - sumatoria2);
if (sumatoria > sumatoria2)
{
MessageBox.Show("Le faltan " + (sumatoria - sumatoria2));
}
decimal a = (sumatoria2 - sumatoria);
if (sumatoria2 > sumatoria)
{
MessageBox.Show("Le Sobran " + a);
lbl1.Text = "1";
}
if (sumatoria == sumatoria2)
{
MessageBox.Show("El numero esta exacto");
lbl1.Text = "1";
}
if ((lbl1.Text == "1") && (lbl2.Text == "1") && (lbl3.Text == "1"))
{
button12.Visible = true;
DAO.Oden_ProduccionDAO producciondao = new GrupoSM_Recepcion.DAO.Oden_ProduccionDAO();
producciondao.idorden = int.Parse(lbl_idorden.Text);
dataGridView6.DataSource = producciondao.ver_producciondetalle();
}	          
}

public decimal metrosdecombinacion()
{
DAO.TelasDAO telasdao=new DAO.TelasDAO();
telasdao.produccion=int.Parse(lbl_idorden.Text);
return telasdao.cantidaddemetroscombinacionporordendeproduccion();
}

private void button7_Click(object sender, EventArgs e)
{
            
decimal sumatoria = metrosdecombinacion();

decimal sumatoria2 = 0;

foreach (DataGridViewRow row in dataGridView4.Rows)
{
sumatoria2 += Convert.ToDecimal(row.Cells["Metros"].Value);
}

textBox2.Text = Convert.ToString(sumatoria - sumatoria2);

if (sumatoria > sumatoria2)
{
MessageBox.Show("Le faltan " + (sumatoria - sumatoria2));
}
decimal a = (sumatoria2 - sumatoria);
if (sumatoria2 > sumatoria)
{
MessageBox.Show("Le Sobran" + a);
lbl2.Text = "1";
}
if (sumatoria == sumatoria2)
{
MessageBox.Show("El numero esta exacto");
lbl2.Text = "1";
}
if ((lbl1.Text == "1") && (lbl2.Text == "1") && (lbl3.Text == "1"))
{
button12.Visible = true;
DAO.Oden_ProduccionDAO producciondao = new GrupoSM_Recepcion.DAO.Oden_ProduccionDAO();
producciondao.idorden = int.Parse(lbl_idorden.Text);
dataGridView6.DataSource = producciondao.ver_producciondetalle();
}
            
            
}

public decimal metrosforro()
{
DAO.TelasDAO telasdao=new DAO.TelasDAO();
telasdao.produccion=int.Parse(lbl_idorden.Text);
return telasdao.cantidaddemetrosforroporordendeproduccion();
}

private void button8_Click(object sender, EventArgs e)
{
            
decimal sumatoria = metrosforro();
decimal sumatoria2 = 0;

foreach (DataGridViewRow row in dataGridView5.Rows)
{
sumatoria2 += Convert.ToDecimal(row.Cells["Metros"].Value);
}

textBox3.Text = Convert.ToString(sumatoria - sumatoria2);

if (sumatoria > sumatoria2)
{
MessageBox.Show("Le faltan " + (sumatoria - sumatoria2));
}
decimal a = (sumatoria2 - sumatoria);
if (sumatoria2 > sumatoria)
{
MessageBox.Show("Le Sobran" + a);
lbl3.Text = "1";
}
if (sumatoria == sumatoria2)
{
MessageBox.Show("El numero esta exacto");
lbl3.Text = "1";
}
if ((lbl1.Text == "1") && (lbl2.Text == "1") && (lbl3.Text == "1"))
{
button12.Visible = true;
DAO.Oden_ProduccionDAO producciondao2 = new GrupoSM_Recepcion.DAO.Oden_ProduccionDAO();
producciondao2.idorden = int.Parse(lbl_idorden.Text);
dataGridView6.DataSource = producciondao2.ver_producciondetalle();
}
           
            
}

private void button9_Click(object sender, EventArgs e)
{
try
{

                
DAO.TelasDAO telasdao2 = new GrupoSM_Recepcion.DAO.TelasDAO();
telasdao2.tipo = 1;
telasdao2.produccion = Convert.ToInt32(dataGridView3.CurrentRow.Cells["produccion"].Value);
//telasdao2.borra_telasasignadas();
//dataGridView3.DataSource = telasdao2.vertelas_asignados();
}
catch
{
MessageBox.Show("Error, escoga una tela, o agregue una");
}
           
            
            

            

}

private void button10_Click(object sender, EventArgs e)
{
try
{

                
DAO.TelasDAO telasdao2 = new GrupoSM_Recepcion.DAO.TelasDAO();
telasdao2.tipo = 2;
telasdao2.produccion = Convert.ToInt32(dataGridView4.CurrentRow.Cells["produccion"].Value);
//telasdao2.borra_telasasignadas();
//dataGridView4.DataSource = telasdao2.vertelas_asignados();
}
catch
{
MessageBox.Show("Error, escoga una tela, o agregue una");
}
           
}

private void button11_Click(object sender, EventArgs e)
{
try
{

DAO.TelasDAO telasdao2 = new GrupoSM_Recepcion.DAO.TelasDAO();
telasdao2.tipo = 3;
telasdao2.produccion = Convert.ToInt32(dataGridView5.CurrentRow.Cells["produccion"].Value);
//telasdao2.borra_telasasignadas();
//dataGridView4.DataSource = telasdao2.vertelas_asignados();
}
catch
{
MessageBox.Show("Error, escoga una tela, o agregue una");
}
            
}

private void button12_Click(object sender, EventArgs e)
{

DAO.Oden_ProduccionDAO producciondao = new GrupoSM_Recepcion.DAO.Oden_ProduccionDAO();
producciondao.idorden = int.Parse(lbl_idorden.Text);
string resultado = producciondao.actualizaordendeproduccionateneralmacendetelascompleto();
if (resultado == "Correcto")
{
MessageBox.Show(resultado);
this.Visible = false;
this.Close();
}
else
{
MessageBox.Show("A habido algun error");
}


            
}

private void button1_Click(object sender, EventArgs e)
{
this.Hide();
this.Close();
}

private void button13_Click(object sender, EventArgs e)
{
try
{
DAO.TelasDAO telasdao = new GrupoSM_Recepcion.DAO.TelasDAO ();
telasdao.id_tela_produccion=Convert.ToInt32 ( dataGridView3.CurrentRow.Cells["id_telaalmacen"].Value );
telasdao.vaciatelabodega ();
cargadatagrids();
}
catch
{
MessageBox.Show ( "Error, escoga una tela, o agregue una" );
}

}

private void button14_Click(object sender, EventArgs e)
{
try
{
DAO.TelasDAO telasdao = new GrupoSM_Recepcion.DAO.TelasDAO();
telasdao.id_tela_produccion=Convert.ToInt32 ( dataGridView4.CurrentRow.Cells["id_telaalmacen"].Value );
telasdao.vaciatelabodega();
cargadatagrids();               
}
catch
{
MessageBox.Show("Error, escoga una tela, o agregue una");
}
}

private void button15_Click(object sender, EventArgs e)
{
try
{
DAO.TelasDAO telasdao = new GrupoSM_Recepcion.DAO.TelasDAO ();
telasdao.id_tela_produccion=Convert.ToInt32 ( dataGridView5.CurrentRow.Cells["id_telaalmacen"].Value );
telasdao.vaciatelabodega ();
cargadatagrids();
}
catch
{
MessageBox.Show ( "Error, escoga una tela, o agregue una" );
}
}

private void button16_Click(object sender, EventArgs e)
{
lbl1.Text = "1";
                
if ((lbl1.Text == "1") && (lbl2.Text == "1") && (lbl3.Text == "1"))
{
button12.Visible = true;
DAO.Oden_ProduccionDAO producciondao = new GrupoSM_Recepcion.DAO.Oden_ProduccionDAO();
producciondao.idorden = int.Parse(lbl_idorden.Text);
dataGridView6.DataSource = producciondao.ver_producciondetalle();
}
}

private void button17_Click(object sender, EventArgs e)
{
             
lbl2.Text = "1";
                
if ((lbl1.Text == "1") && (lbl2.Text == "1") && (lbl3.Text == "1"))
{
button12.Visible = true;
DAO.Oden_ProduccionDAO producciondao = new GrupoSM_Recepcion.DAO.Oden_ProduccionDAO();
producciondao.idorden = int.Parse(lbl_idorden.Text);
dataGridView6.DataSource = producciondao.ver_producciondetalle();
}
}

private void button18_Click(object sender, EventArgs e)
{
lbl3.Text = "1";
                
if ((lbl1.Text == "1") && (lbl2.Text == "1") && (lbl3.Text == "1"))
{
button12.Visible = true;
DAO.Oden_ProduccionDAO producciondao2 = new GrupoSM_Recepcion.DAO.Oden_ProduccionDAO();
producciondao2.idorden = int.Parse(lbl_idorden.Text);
dataGridView6.DataSource = producciondao2.ver_producciondetalle();
}
}
}
}
