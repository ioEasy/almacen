﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrupoSM_Recepcion.Services
{
class AviosAlmacen
{
public class AvioAlmacen
{
public AvioAlmacen(string nombre, string tipo, decimal cantidad, int ID)
{
this.Nombre = nombre;
this.Tipo = tipo;
this.Cantidad = cantidad;
this.ID = ID;                
}

//the next attribute prevents the EmployeeId column from showing
//[Browsable(false)]
public string Tipo { get; set; }
public string Nombre { get; set; }
public int ID { get; set; }
public Decimal Cantidad { get; set; }
}
public class AvioBodega
{
public AvioBodega(string nombre, string tipo, decimal cantidad, string Color)
{
this.Nombre = nombre;
this.Tipo = tipo;
this.Cantidad = cantidad;
this.Color = Color;
}

//the next attribute prevents the EmployeeId column from showing
//[Browsable(false)]
public string Tipo { get; set; }
public string Nombre { get; set; }
public string Color { get; set; }
public Decimal Cantidad { get; set; }
}
public class AvioBodegaAlmacen
{
public AvioBodegaAlmacen(string Nombre,string Color,decimal Requisionado,decimal Bodega,decimal Comprar)
{
this.Nombre=Nombre;
this.Color=Color;
this.Requisionado=Requisionado;
this.Bodega=Bodega;
this.Comprar=Comprar;
}

//the next attribute prevents the EmployeeId column from showing
//[Browsable(false)]
public string Nombre { get; set; }
public string Color { get; set; }
public Decimal Requisionado { get; set; }
public Decimal Bodega { get; set; }
public Decimal Comprar { get; set; }
}

}
}
