﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using System.Resources;

namespace GrupoSM_Recepcion.Services
{
class TelaRequisicion
{
public class Tela
{
public decimal Metros { get; set; }
public decimal Ancho { get; set; }
public string Nombre { get; set; }
public string Color { get; set; }

public Tela(string Nombre, decimal Ancho, decimal Metros, string Color)
{
this.Nombre = Nombre;
this.Ancho = Ancho;
this.Metros = Metros;
this.Color = Color;
}

}
public class Combinacion
{
public decimal Metros { get; set; }
public decimal Ancho { get; set; }
public string Nombre { get; set; }
public string Color { get; set; }

public Combinacion(string Nombre, decimal Ancho, decimal Metros, string Color)
{
this.Nombre = Nombre;
this.Ancho = Ancho;
this.Metros = Metros;
this.Color = Color;
}

}
public class Forro
{
public decimal Metros { get; set; }
public decimal Ancho { get; set; }
public string Nombre { get; set; }
public string Color { get; set; }

public Forro(string Nombre, decimal Ancho, decimal Metros, string Color)
{
this.Nombre = Nombre;
this.Ancho = Ancho;
this.Metros = Metros;
this.Color = Color;
}
}
}
}
    


