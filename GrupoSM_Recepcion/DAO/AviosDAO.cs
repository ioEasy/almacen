﻿using System;
using System.Linq;
using System.Data;

namespace GrupoSM_Recepcion.DAO
{
class AviosDAO
{
BO.DS_MasterDataSetTableAdapters.seleccionatallasdelaordendeproduccionTableAdapter devuelvetallasdelasordenesdeproduccion=new BO.DS_MasterDataSetTableAdapters.seleccionatallasdelaordendeproduccionTableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelvevistadeingresosaalmacendebodegaTableAdapter aviosvistabodegaalmaceningreso=new BO.DS_MasterDataSetTableAdapters.devuelvevistadeingresosaalmacendebodegaTableAdapter();

BO.DS_MasterDataSetTableAdapters.AviosBodega_FichaTecnicaTableAdapter aviosbodegafichatecnica=new BO.DS_MasterDataSetTableAdapters.AviosBodega_FichaTecnicaTableAdapter();

//BO.DS_MasterDataSetTableAdapters.aviosnombreTableAdapter nombreavios = new BO.DS_MasterDataSetTableAdapters.aviosnombreTableAdapter();
//BO.DS_MasterDataSetTableAdapters.aviosproducciontablaTableAdapter aviosproducciont = new BO.DS_MasterDataSetTableAdapters.aviosproducciontablaTableAdapter();
//BO.DS_MasterDataSetTableAdapters.devuelvenumeroavioprodTableAdapter numeroavioproduccion = new BO.DS_MasterDataSetTableAdapters.devuelvenumeroavioprodTableAdapter();

BO.DS_MasterDataSetTableAdapters.AviosAlmacen_DetalleTableAdapter tablaaviosalmacen = new BO.DS_MasterDataSetTableAdapters.AviosAlmacen_DetalleTableAdapter();

BO.DS_MasterDataSetTableAdapters.AviosAlmacenTableAdapter tablaaviosalmacendetalle = new BO.DS_MasterDataSetTableAdapters.AviosAlmacenTableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelveavioshojacorteeTableAdapter devuelveavioshojacorte = new BO.DS_MasterDataSetTableAdapters.devuelveavioshojacorteeTableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelvenumaviosdetallealmacenTableAdapter numeroaviosdetallealmacen = new BO.DS_MasterDataSetTableAdapters.devuelvenumaviosdetallealmacenTableAdapter();

//BO.DS_MasterDataSetTableAdapters.devuelvevistabodegaaviostipo2TableAdapter vistabodegareportetipo = new BO.DS_MasterDataSetTableAdapters.devuelvevistabodegaaviostipo2TableAdapter();

BO.DS_MasterDataSetTableAdapters.ver_avios_fichasTableAdapter ver_aviosficha = new BO.DS_MasterDataSetTableAdapters.ver_avios_fichasTableAdapter();

//BO.DS_MasterDataSetTableAdapters.vistabodegaavios2TableAdapter vistabodegadeavios = new BO.DS_MasterDataSetTableAdapters.vistabodegaavios2TableAdapter();

//BO.DS_MasterDataSetTableAdapters.aviosTableAdapter tablaavios = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.aviosTableAdapter();

//BO.DS_MasterDataSetTableAdapters.busca_aviostipoBASEPortipoTableAdapter tablaaviosbasetipo = new BO.DS_MasterDataSetTableAdapters.busca_aviostipoBASEPortipoTableAdapter();

//BO.DS_MasterDataSetTableAdapters.avios_detalleTableAdapter tabladetalleavios = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.avios_detalleTableAdapter();

BO.DS_MasterDataSetTableAdapters.QueriesTableAdapter querysadapter = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.QueriesTableAdapter();

//BO.DS_MasterDataSetTableAdapters.avios_bodegaTableAdapter tablabodega = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.avios_bodegaTableAdapter();

//BO.DS_MasterDataSetTableAdapters.numeroaviosTableAdapter numeroavio = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.numeroaviosTableAdapter();

//BO.DS_MasterDataSetTableAdapters.avios_mas_bodegaTableAdapter aviosvista = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.avios_mas_bodegaTableAdapter();

//BO.DS_MasterDataSetTableAdapters.avios_produccionTableAdapter tablaaviosproduccion = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.avios_produccionTableAdapter();

//BO.DS_MasterDataSetTableAdapters.vistabodegaavios3TableAdapter vistabodegaaviosfaltantes = new BO.DS_MasterDataSetTableAdapters.vistabodegaavios3TableAdapter();

//BO.DS_MasterDataSetTableAdapters.vistabodegaavios4TableAdapter vistabodegaavioscatalogo = new BO.DS_MasterDataSetTableAdapters.vistabodegaavios4TableAdapter();

BO.DS_MasterDataSetTableAdapters.Existen_aviosproduccionTableAdapter existeavioproduccion = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.Existen_aviosproduccionTableAdapter();

//BO.DS_MasterDataSetTableAdapters.avios_orden_asignacionesTableAdapter aviostablaasignaciones = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.avios_orden_asignacionesTableAdapter();

//BO.DS_MasterDataSetTableAdapters.busca_aviostipoTableAdapter aviostipo = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.busca_aviostipoTableAdapter();

//BO.DS_MasterDataSetTableAdapters.devuelveaviosproduccionTableAdapter aviosproduccion = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.devuelveaviosproduccionTableAdapter();

//BO.DS_MasterDataSetTableAdapters.busca_tipo_aviosmasbodegaTableAdapter tipomasbodega = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.busca_tipo_aviosmasbodegaTableAdapter();

//BO.DS_MasterDataSetTableAdapters.devuelveavios_plantillaTableAdapter aviosplantilla;

//BO.DS_MasterDataSetTableAdapters.numeroaviosproduccionTableAdapter numeroaviosproduccion;

//BO.DS_MasterDataSetTableAdapters.ColorAvioTableAdapter tablacoloravios = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.ColorAvioTableAdapter();

//BO.DS_MasterDataSetTableAdapters.avios_impresionTableAdapter impresionavios;

 BO.DS_MasterDataSetTableAdapters.devuelvevistadeingresosaalmacendebodegaTableAdapter aviosasignacion = new BO.DS_MasterDataSetTableAdapters.devuelvevistadeingresosaalmacendebodegaTableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelvenumeroprendascolorTableAdapter numeroprendascolor;


BO.DS_MasterDataSetTableAdapters.devuelvealmacenavios01TableAdapter almacenavios = new BO.DS_MasterDataSetTableAdapters.devuelvealmacenavios01TableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelvedatosalmacenaviosTableAdapter datosalmacenavios = new BO.DS_MasterDataSetTableAdapters.devuelvedatosalmacenaviosTableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelvetallasaviosTableAdapter tallasproduccion = new BO.DS_MasterDataSetTableAdapters.devuelvetallasaviosTableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelvenumerodealmacenTableAdapter numerodealmacen = new BO.DS_MasterDataSetTableAdapters.devuelvenumerodealmacenTableAdapter();

//BO.DS_MasterDataSetTableAdapters.devuelvenumeroavioalmacendetalleTableAdapter numeroavioalmacendetalle = new BO.DS_MasterDataSetTableAdapters.devuelvenumeroavioalmacendetalleTableAdapter();
  //BO.DS_MasterDataSetTableAdapters.devuelveaviosdelasfichastecnicasTableAdapter aviosdelasfichastecnicas=new BO.DS_MasterDataSetTableAdapters.devuelveaviosdelasfichastecnicasTableAdapter();
  BO.DS_MasterDataSetTableAdapters.devuelvedatoscatalogoalmacenaviosTableAdapter catalogoalmacendatospornombre = new BO.DS_MasterDataSetTableAdapters.devuelvedatoscatalogoalmacenaviosTableAdapter();

BO.DS_MasterDataSetTableAdapters.catalogodealmacenexistenciasaviosTableAdapter catalogolmacenexistencias = new BO.DS_MasterDataSetTableAdapters.catalogodealmacenexistenciasaviosTableAdapter();

BO.DS_MasterDataSetTableAdapters.AviosEntradasBodega_DetalleTableAdapter tablaaviosentradasbodegadetalle=new BO.DS_MasterDataSetTableAdapters.AviosEntradasBodega_DetalleTableAdapter();

BO.DS_MasterDataSetTableAdapters.seleccionacoloresdelaordendeproduccionTableAdapter devuelvecoloresordendeproduccion=new BO.DS_MasterDataSetTableAdapters.seleccionacoloresdelaordendeproduccionTableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelveaviosrequisicionbodegaTableAdapter aviosrequisicionbodega=new BO.DS_MasterDataSetTableAdapters.devuelveaviosrequisicionbodegaTableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelveaviosdelasfichastecnicasnuevatablaTableAdapter aviosfichastecnicasnuevatabla=new BO.DS_MasterDataSetTableAdapters.devuelveaviosdelasfichastecnicasnuevatablaTableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelveaviosplantillasnuevatablaTableAdapter plantillaaviosnuevatabla=new BO.DS_MasterDataSetTableAdapters.devuelveaviosplantillasnuevatablaTableAdapter();

BO.DS_MasterDataSetTableAdapters.AviosPlantillasTableAdapter aviosplantillasnuevatabla=new BO.DS_MasterDataSetTableAdapters.AviosPlantillasTableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelvenumeroaviosentradaabodegaTableAdapter numeroaviosentradabodega=new BO.DS_MasterDataSetTableAdapters.devuelvenumeroaviosentradaabodegaTableAdapter();
BO.DS_MasterDataSetTableAdapters.devuelvenumerodebodegaaviosTableAdapter numerodebodegaavios=new BO.DS_MasterDataSetTableAdapters.devuelvenumerodebodegaaviosTableAdapter();

public int IDAvioAlmacen { get; set; }
public int id_ficha_avio { get; set; }
public int idavios { get; set; }
public int idplantilla { get; set; }
public string nombre { get; set; }
public string Color { get; set; }
public int tipo { get; set; }
public string tipoo { get; set; }
public string talla { get; set; }
public double precio { get; set; }
public int iddetalle { get; set; }
public decimal requisicionado { get; set; }
public decimal cantidadd { get; set; }
public double cantidad { get; set; }
public decimal cantidadbodegaa { get; set; }
public double costo { get; set; }
public int idaviosbodega { get; set; }
public int idproduccion { get; set; }
public decimal cantidadasignada { get; set; }
public double cantidadbodega { get; set; }
public DateTime fecha { get; set; }
public double cantidad_ficha { get; set; }
public int idavioproduccion { get; set; }

public string actualizaaproduccionavioscompletos()
{
querysadapter.actualizaavioscompletosaordenesdeproduccion(this.idproduccion);
return "Correcto";
} 

public decimal numerobodegaavios()
{
return Convert.ToDecimal(numerodebodegaavios.GetData(this.idavios).Max().Cantidad);
}

public DataTable tallasordenesdeproduccion()
{
return devuelvetallasdelasordenesdeproduccion.GetData(this.idproduccion);
}

public string eliminaaviosrequisicion()
{
querysadapter.eliminarequisiciondeavios(this.idproduccion);
return "Correcto";
}

public DataTable aviosdelasfichastecnicasnuevatabla()
{
return aviosfichastecnicasnuevatabla.GetData(this.id_ficha_avio);
}

public DataTable devuelverequisiciontabla()
{
return aviosrequisicionbodega.GetData(this.idproduccion);
}

public DataTable devuelvecoloresdelasordenesdeproduccion()
{
return devuelvecoloresordendeproduccion.GetData(this.idproduccion);
}

public int devuelvenumerodeingresoaaviosentradabodega()
{ 
int numero=Convert.ToInt32(numeroaviosentradabodega.GetData().Max().Column1);
  return numero;
}

public void ingresadetalleingresoaviosabodega()
{
querysadapter.ingresadetalleentradasabodegadeavios(this.idproduccion, devuelvenumerodeingresoaaviosentradabodega());
}

public string insertaaviosentradasbodega()
{
querysadapter.ingresaaviosbodegaingresos(this.idavios, this.requisicionado, this.fecha, this.talla, this.Color);
ingresadetalleingresoaviosabodega();
return "Correcto";
}

public string ingresanuevatabladeaviosdetalle()
{
aviosbodegafichatecnica.Insert(this.idavios, this.id_ficha_avio, this.cantidad);
return "Correcto";
}

//public DataTable devuelvetabladetalleavios()
//{
//return tabladetalleavios.GetData();
//}

//public DataTable devuelveaviosporfichatecnica()
//{
//return aviosdelasfichastecnicas.GetData( this.id_ficha_avio );
//}

public string actualizaordendeproduccionalmacencompleto()
{
querysadapter.actualizaordenproduccionaalmacenavioscompleto(this.idproduccion);
return "Correcto";
}

//public DataTable aviosproducciontablanombre()
//{
//return nombreavios.GetData(this.idavios);
//}
				
//public DataTable aviosproducciontabla()
//{
//return aviosproducciont.GetData();
//}

public DataTable existenciascatalogoalmacenavios()
{
return catalogolmacenexistencias.GetData();
}


public DataTable datoscatalogoalmacenavios()
{
return catalogoalmacendatospornombre.GetData(this.nombre);
}

public string eliminadebodegaalmacen()
{
querysadapter.eliminaalmacenbodega(this.IDAvioAlmacen);
return "Correcto";
}

public DataTable devuelvehojadecorteavios()
{
return devuelveavioshojacorte.GetData(this.idproduccion);
}

public string ingresaalmacencatalogo()
{
querysadapter.ingresacantidaddebodegaavios(this.cantidadbodegaa, this.cantidadd, this.idavioproduccion);
return "Correcto";
}

//public string insertalmacendetalle()
//{
//querysadapter.insertaalmacendetalle(this.idavios, this.idproduccion, this.cantidadd);
//insertadetallealmacen();
//return "Correcto";
//}

public int devuelvenumerodealmacen()
{
return Convert.ToInt32(numerodealmacen.GetData().Max().Column1);
}

//public int numeroaviosalmacendetalle()
//{
//return Convert.ToInt32(numeroavioalmacendetalle.GetData().Max().Column1);
//}

public string ingresaprimeralmacen()
{
querysadapter.ingresaalmacenbodegaavios(this.idavioproduccion, this.cantidadd, this.fecha);
return "Correcto";
}
  //public void insertadetallealmacen()
  //{
  //querysadapter.insertaproducciondetalleavios(numeroaviosalmacendetalle(), this.Color, this.talla);
  //}

  public string insertaalmacen()
{ 
querysadapter.ingresaalmacen(this.cantidadd, this.idproduccion);
querysadapter.ingresacatalogo(devuelvenumerodealmacen(), this.nombre, this.tipoo);
return "Correcto";
}
        
public DataTable devuelvetallasproduccion()
{
return tallasproduccion.GetData(this.idproduccion);
}
public DataTable datosalmacen()
{
return datosalmacenavios.GetData(this.nombre);
}

public DataTable almacenavioslistado()
{
return almacenavios.GetData();
}

       
public string eliminaentradaavios()
{
querysadapter.eliminaaviosalmacen(this.IDAvioAlmacen);
return "Correcto";
}
public int numeros_almacen()
{
try
{
return Convert.ToInt32(numeroaviosdetallealmacen.GetData().Max().Column1);
}
catch
{
return 100;
}
}
        
        
        
public DataTable devuelvealmacenavio()
{
return almacenavios.GetData();
}

       
//public DataTable devuelvebodegaaviosparaimprimir()
//{
//return vistabodegadeavios.GetData();
//}
//public DataTable devuelvefaltantesaviosbodega()
//{
//return vistabodegaaviosfaltantes.GetData();
//}

//public DataTable devuelvecatalogoaviosbodega()
//{
//return vistabodegaavioscatalogo.GetData();
//}

public string Actualizaaviosproduccion()
{
            
querysadapter.actualizaavioproduccionsubgrupo(this.idavios, this.iddetalle);
return "Correcto";
            
}

//public DataTable vistaimpresionbodegaaviosportipo()
//{
//return vistabodegareportetipo.GetData(this.tipo);
//}
public string eliminaaviossubgruposcolor()
{
            
querysadapter.eliminaaviossubgrupos(this.idavios);
return "Correcto";
            
}


//public DataTable devuelveaviostipobase()
//{
//BO.DS_MasterDataSetTableAdapters.busca_aviostipoBASETableAdapter aviosbase = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.busca_aviostipoBASETableAdapter();
//return aviosbase.GetData();
//}

public int numerocolorprendas()
{
numeroprendascolor = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.devuelvenumeroprendascolorTableAdapter();
return Convert.ToInt32(numeroprendascolor.GetData(this.idproduccion, this.Color).Max().Cantidad_prendas);

}

public DataTable devuelveaviosasignaciones()
{

return aviosasignacion.GetData(this.idproduccion);

}

public DataTable avios_asignacionbodega()
{
aviosasignacion = new BO.DS_MasterDataSetTableAdapters.devuelvevistadeingresosaalmacendebodegaTableAdapter();
return aviosasignacion.GetData(this.idproduccion);
}

//public DataTable aviosimpresion()
//{
//impresionavios = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.avios_impresionTableAdapter();
//return impresionavios.GetData(this.idproduccion);
//}

//public DataTable devuelveplantillaavios()
//{
//aviosplantilla=new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.devuelveavios_plantillaTableAdapter();
//return aviosplantilla.GetData(this.idplantilla);
//}

public string insertaaviosplantilla()
{
            
querysadapter.insertaplantilla_avios(this.idplantilla, this.idavios, this.cantidad, this.costo);
return "Correcto";
            
}

public string eliminaaviosplantilla()
{
           
querysadapter.eliminaplantilla_avios(this.idplantilla, this.idavios);
return "Correcto";
            
}

//public DataTable aviosproduc()
//{
//return aviosproduccion.GetData(this.idproduccion);
//}

public string actualizabodegaavios()
{
int numero = Convert.ToInt32(this.cantidad);
querysadapter.actualizabodegaavios(this.idavios, numero);
return "Correcto";
            
}

public string eliminaaviosproduccion()
{
            
querysadapter.eliminaaviosproduccion(this.idproduccion);
return "Correcto";
            
}

public string actualizabodega_avios()
{
querysadapter.actualiza_bodegaavios(this.idaviosbodega, Math.Round((this.cantidad), 2));
return "Correcto";
}

//public DataTable aviosbodega()
////{
////return aviosvista.GetData();
//}
////public int numeroavioalmacen()
//{
////return Convert.ToInt32(numeroavio.GetData().Max().Column1);
//}
//public int numeros_avios()
//{
//return Convert.ToInt32(numeroavio.GetData().Max().Column1);
//}

//public int numero_avios_produccion()
//{
//numeroaviosproduccion = new GrupoSM_Recepcion.BO.DS_MasterDataSetTableAdapters.numeroaviosproduccionTableAdapter();
//return Convert.ToInt32(numeroaviosproduccion.GetData().Max().Column1);
//}

       

public DataTable sacar_avios()
{
return ver_aviosficha.GetData(this.id_ficha_avio);
}


public string insertabodega()
{

//tablabodega.Insert(idavios, this.cantidad);
return "correcto";

}


public string agregar_avios()
{
//tablaavios.Insert(this.nombre, this.tipo, Convert.ToDouble(this.precio));
return "Agregado Correctamente";
            
}

public string agregar_detalle()
{
querysadapter.insertaficha_avios(this.id_ficha_avio, this.idavios, Math.Round((this.cantidad), 2), this.costo);
return "Agregado Correctamente";
            
}

//public DataTable sacartodosavios()
//{
//return tablaavios.GetData();
//}

public string actualizaavios()
{
querysadapter.actualiza_avios(this.idavios, this.nombre, this.tipo, this.precio);
return "Actualizado";
           
}

        

public int existe_produccionavios()
{
return Convert.ToInt32(existeavioproduccion.GetData(this.idproduccion).Max().Return_Status);
}

        

public int verificaavios()
{
int respuesta = querysadapter.verificaordenavios(this.idproduccion);          
return respuesta;            
}


        

//public DataTable busca_aviosportipo()
//{
//return aviostipo.GetData(this.tipo);
//}

//public DataTable busca_aviosportipobase()
//{
//return tablaaviosbasetipo.GetData(this.tipo); 
//}

public string borra_avioficha()
{
            
querysadapter.borra_aviosficha(this.idavios, this.id_ficha_avio);
return "Borrado";
            
}

//public DataTable buscatipobodega()
//{
//return tipomasbodega.GetData(this.tipo);
//}

}
}
