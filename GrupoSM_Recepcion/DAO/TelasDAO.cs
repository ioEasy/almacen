﻿using System;
using System.Data;
using GrupoSM_Recepcion.Services;
using System.Linq;

namespace GrupoSM_Recepcion.DAO
{
class TelasDAO
{  BO.DS_MasterDataSetTableAdapters.devuelvetelasrequisicionTableAdapter telasrequisicionpororden = new BO.DS_MasterDataSetTableAdapters.devuelvetelasrequisicionTableAdapter();

BO.DS_MasterDataSetTableAdapters.devuelveforrosrequisicionTableAdapter devuelveforrosrequisicion=new BO.DS_MasterDataSetTableAdapters.devuelveforrosrequisicionTableAdapter();
  
BO.DS_MasterDataSetTableAdapters.devuelvecombinacionessrequisicionTableAdapter devuelvecombinacionesrequisicion=new BO.DS_MasterDataSetTableAdapters.devuelvecombinacionessrequisicionTableAdapter();

BO.DS_MasterDataSetTableAdapters.TelasRequisicionTableAdapter telasrequisicion=new
BO.DS_MasterDataSetTableAdapters.TelasRequisicionTableAdapter();
BO.DS_MasterDataSetTableAdapters.CombinacionesRequisicionTableAdapter combinacionrequisicion=new BO.DS_MasterDataSetTableAdapters.CombinacionesRequisicionTableAdapter();
BO.DS_MasterDataSetTableAdapters.ForrosRequisicionTableAdapter forrosrequisicion=new BO.DS_MasterDataSetTableAdapters.ForrosRequisicionTableAdapter();
BO.DS_MasterDataSetTableAdapters.devuelveidtelabodegaTableAdapter numerodebodegatela = new BO.DS_MasterDataSetTableAdapters.devuelveidtelabodegaTableAdapter ();
  BO.DS_MasterDataSetTableAdapters.devuelvetelasbodegadeproduccionhojadecorteTableAdapter telasproduccionhojacorte = new BO.DS_MasterDataSetTableAdapters.devuelvetelasbodegadeproduccionhojadecorteTableAdapter ();
  BO.DS_MasterDataSetTableAdapters.devuelvetelasbodegaportipodeproduccionTableAdapter telasproduccionportipo = new BO.DS_MasterDataSetTableAdapters.devuelvetelasbodegaportipodeproduccionTableAdapter ();
  BO.DS_MasterDataSetTableAdapters.devuelvetelasproduccionTableAdapter telasproduccion = new BO.DS_MasterDataSetTableAdapters.devuelvetelasproduccionTableAdapter();
BO.DS_MasterDataSetTableAdapters.telacosteoTableAdapter tablatelacosteo = new BO.DS_MasterDataSetTableAdapters.telacosteoTableAdapter();
BO.DS_MasterDataSetTableAdapters.QueriesTableAdapter queriesadpter = new BO.DS_MasterDataSetTableAdapters.QueriesTableAdapter();
BO.DS_MasterDataSetTableAdapters.devuelvetelasproduccionTableAdapter tablatelasalmacen = new BO.DS_MasterDataSetTableAdapters.devuelvetelasproduccionTableAdapter();
BO.DS_MasterDataSetTableAdapters.devuelvemetrosdeteladeunaordendeproduccionTableAdapter metrostelaprocedure=new BO.DS_MasterDataSetTableAdapters.devuelvemetrosdeteladeunaordendeproduccionTableAdapter();
BO.DS_MasterDataSetTableAdapters.devuelvemetrosdecombinaciondeunaordendeproduccionTableAdapter metroscombinacionrequisicionordendeproduccion=new BO.DS_MasterDataSetTableAdapters.devuelvemetrosdecombinaciondeunaordendeproduccionTableAdapter();
BO.DS_MasterDataSetTableAdapters.devuelvemetrosdeforrodeunaordendeproduccionTableAdapter metrosdeforrorequisicionordendeproduccion=new BO.DS_MasterDataSetTableAdapters.devuelvemetrosdeforrodeunaordendeproduccionTableAdapter();

public int idtela_bodega { get; set; }
public int cliente { get; set; }
public int proveedor { get; set; }
public DateTime fecha_entrada { get; set; }
public string nombre_descripcion { get; set; }
public decimal metros { get; set; }
public decimal metross { get; set; }
public string composicion { get; set; }
public string color { get; set; }
public decimal ancho { get; set; }
public decimal anchoo { get; set; }
public int tipo { get; set; }
public int idtelacosteo { get; set; }
public string nombretelacosteo { get; set; }
public decimal precio { get; set; }
public int id_tela_produccion { get; set; }
public int produccion { get; set; }
public DateTime fecha_entrada_produccion { get; set; }
public decimal largo_trazo { get; set; }
public decimal paños { get; set; }
public decimal utilizado_tela { get; set; }
public decimal retazo_tela { get; set; }
public decimal saldo_tela { get; set; }
public decimal faltante_tela { get; set; }
public decimal precio_metro { get; set; }
public string tipoo { get; set; }

public DataTable telasrequisicionorden()
{
return telasrequisicionpororden.GetData(this.produccion);
}

public DataTable combinacionesrequisicionorden()
{
return devuelvecombinacionesrequisicion.GetData(this.produccion);
}

public DataTable forrosrequisicionorden()
{
return devuelveforrosrequisicion.GetData(this.produccion);
}

public string telarequisiciones()
{
telasrequisicion.Insert(this.produccion,this.anchoo, this.metros, this.nombre_descripcion, this.color);
return "Correcto";
}


public string combinacionesrequisiciones()
{
combinacionrequisicion.Insert(this.produccion,this.anchoo, this.metros, this.nombre_descripcion, this.color);
return "Correcto";
}


public string forrorequisiciones()
{
 forrosrequisicion.Insert(this.produccion,this.ancho,this.metros,this.nombre_descripcion,this.color);
return "Correcto";
}



public decimal cantidaddemetrosforroporordendeproduccion()
{
return Convert.ToDecimal(metrosdeforrorequisicionordendeproduccion.GetData(this.produccion).Max().Column1);
}

public decimal cantidaddemetroscombinacionporordendeproduccion()
{
  return Convert.ToDecimal( metroscombinacionrequisicionordendeproduccion.GetData( this.produccion ).Max().Column1);
}



public decimal cantidaddemetrosdetelaporordendeproduccion()
{
 return Convert.ToDecimal(metrostelaprocedure.GetData(this.produccion).Max().Column1);
}

public DataTable devuelvetelashojadecorte()
{
return telasproduccionhojacorte.GetData(this.id_tela_produccion);
}


  public DataTable telasportipodeproduccion()
  {
  return telasproduccionportipo.GetData(this.tipoo,this.produccion);
  }

  public int numerodeidtelabodegaproduccion()
{
return Convert.ToInt32 ( numerodebodegatela.GetData().Max().Column1);
}

public string vaciatelabodega()
{
queriesadpter.vaciateladebodega ( this.id_tela_produccion );
return "Correcto";
}

public string ingresacatalogobodegatela()
{
int numerodebodegatela = numerodeidtelabodegaproduccion ();
queriesadpter.insertacatalogobodegatelas ( numerodebodegatela,this.nombre_descripcion );
return "Correcto";
}

public DataTable telasrequisiciones()
{
return telasproduccion.GetData(this.produccion);
}
        
public string ingresatelabodega()
{
queriesadpter.insertabodegatelas ( this.produccion,this.fecha_entrada_produccion,this.metros,this.composicion,this.color,
this.anchoo,this.tipoo );
return "Correcto";
}

        

//public string modificatelascosteo()
//{
//    try
//    {
//        queriesadpter.modificatelacosteo(this.nombretelacosteo, this.precio, this.idtelacosteo);
//        return "Correcto";
//    }
//    catch
//    {
//        return "Error de coneccion";
//    }
//}

public string eliminatelascosteo()
{
try
{
queriesadpter.eliminatelacosteo(this.idtelacosteo);
return "Correcto";
}
catch
{
return "Error de coneccion";
}
}        
}
}
